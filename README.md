# This is a working neural network

This network can be adjusted variably to fit any amount of inputs, hidden layers, and outputs.

Things to do:
- Save network into a file/read network from the file
- Incorporate dropout
- Make GUI for network if small enough
- Reinforcement learning uses Q Learning?
- Convolution?
