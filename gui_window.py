from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QLabel, QVBoxLayout, QHBoxLayout, QWidget, QTextEdit
from PyQt5.QtCore import Qt, QThread, pyqtSignal, QObject, QEvent
import sys

# MainWindow class using QMainWindow
class MainWindow(QMainWindow):
    def __init__(self):
        """
        This will build the structures for the main window gui
        """
        super(MainWindow, self).__init__()
        # Main window stylizing properties
        self.setGeometry(50, 50, 1000, 500)
        self.setStyleSheet("background-color: gray")
        self.setWindowTitle("Neural Network GUI")

        # This builds the main widget for the window to hold
        main_widget = QWidget()
        self.setCentralWidget(main_widget)

        # Layout portion of the main window
        vertical_layout = QVBoxLayout()

        vertical_layout.setSpacing(0)
        vertical_layout.setContentsMargins(0, 0, 0, 0)
        self.centralWidget().setLayout(vertical_layout)
        self.centralWidget().isWindow()

        # Shows the main window
        self.show()

    # When the closed button is pushed this function is called
    def closeEvent(self, *args, **kwargs):
        print("Closing")


class NeuralNetworkGUI:
    def __init__(self):
        """
        Initialization of NeuralNetworkGUI to run the neural network using a visual interface
        """
        self.app = None
        self.GUI = None

    def run(self):
        """
        Runs the PyQt5 framework for the GUI interface for the neural network
        :return: {This runs the main application}
        """
        self.app = QApplication(sys.argv)
        self.GUI = MainWindow()
        sys.exit(self.app.exec())

# For now we will run this main as a test and then remove later on once it has been integrated with the neural network
def main():
    x = NeuralNetworkGUI()
    x.run()


if __name__ == "__main__":
    main()
