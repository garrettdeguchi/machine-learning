import numpy as np
import sys
from enum import Enum
from datetime import datetime
import os

class ActivationFunction(Enum):
    sigmoid = 0
    relu = 1
    leaky_relu = 2


class PropagationType(Enum):
    forward = 0
    backward = 1


WEIGHTS_LOW = -1
WEIGHTS_HIGH = 1

BIAS_LOW = -1
BIAS_HIGH = 1

RELU_DERIVATIVES = 1
LEAKY_RELU_DERIVATIVES = [1, 0.01]

LEARNING_RATE = -0.5

ACTIVATION = ActivationFunction.sigmoid

INPUT_LAYER = 2
HIDDEN_LAYER_1 = 2
HIDDEN_LAYER_2 = 2
OUTPUT_LAYER = 2

NEURAL_NETWORK_LAYERS = [INPUT_LAYER, HIDDEN_LAYER_1, OUTPUT_LAYER]


class NeuralNetwork:
    def __init__(self, input_architecture):
        """
        Initialization of the neural network
        :param input_architecture:
        """
        self.architecture = input_architecture
        self.weights = []
        self.bias = []

    def sigmoid_activation(self, input_layer, input_propagation_type):
        """
        Takes the input layer and runs it through the sigmoid function. If the input_propagation_type is
        PropagationType.forward then it does the normal sigmoid, otherwise it does the derivative of the sigmoid
        :param input_layer:
        :param input_propagation_type: {PropagationType.forward | PropagationType.backward}
        :return: {input_layer run through the sigmoid function}
        """
        if(input_propagation_type == PropagationType.forward):
            return 1.0/(1.0 + np.exp(-input_layer))
        elif(input_propagation_type == PropagationType.backward):
            return np.multiply((1.0/(1.0 + np.exp(-input_layer))), (1.0 - (1.0/(1.0 + np.exp(-input_layer)))))
        else:
            print("PropagationType is not accepted please fix")
            sys.exit()

    def relu_activation(self, input_layer, input_propagation_type):
        """
        Takes the input layer and runs it through the relu function. If the input_propagation_type is
        PropagationType.forward then it does the normal relu, otherwise it does the derivative of the relu
        :param input_layer:
        :param input_propagation_type: {PropagationType.forward | PropagationType.backward}
        :return: {input_layer run through the relu function}
        """
        if(input_propagation_type == PropagationType.forward):
            input_layer[input_layer <= 0] = 0
            input_layer[input_layer > 0] *= RELU_DERIVATIVES
            return input_layer
        elif(input_propagation_type == PropagationType.backward):
            input_layer[input_layer <= 0] = 0
            input_layer[input_layer > 0] = RELU_DERIVATIVES
            return input_layer
        else:
            print("PropagationType is not accepted please fix :: relu")
            sys.exit()

    def leaky_relu_activation(self, input_layer, input_propagation_type):
        """
        Takes the input layer and runs it through the leaky relu function. If the input_propagation_type is
        PropagationType.forward then it does the normal leaky relu, otherwise it does the derivative of the leaky
        relu
        :param input_layer:
        :param input_propagation_type: {PropagationType.forward | PropagationType.backward}
        :return: {input_layer run through the leaky relu function}
        """
        if(input_propagation_type == PropagationType.forward):
            input_layer[input_layer <= 0] *= LEAKY_RELU_DERIVATIVES[1]
            input_layer[input_layer > 0] *= LEAKY_RELU_DERIVATIVES[0]
            return input_layer
        elif(input_propagation_type == PropagationType.backward):
            input_layer[input_layer <= 0] = LEAKY_RELU_DERIVATIVES[1]
            input_layer[input_layer > 0] = LEAKY_RELU_DERIVATIVES[0]
            return input_layer
        else:
            print("PropagationType is not accepted please fix :: relu")
            sys.exit()

    def set_weights(self, input_weights):
        """
        This will check if the number of weights are the correct amount according to the
        neural network architecture
        :param input_weights:
        :return: {True, False}
        """
        if(input_weights == None):
            for i in range(1, len(self.architecture)):
                self.weights.append(np.matrix(np.random.uniform(WEIGHTS_LOW, WEIGHTS_HIGH,
                                                                (self.architecture[i-1], self.architecture[i]))))
        else:
            if(len(input_weights) != (len(self.architecture) - 1)):
                print("Weights do not match up with the current architecture")
                return False
            else:
                for i in range(0, len(self.architecture)-1):
                    if((input_weights[i].shape[1] != self.architecture[i]) or
                            (input_weights[i].shape[0] != self.architecture[i-1])):
                        print("Weights do not match up with the current architecture")
                        return False
                self.weights = input_weights
        return True

    def set_biases(self, input_biases):
        """
        This will check if the number of biases are the correct amount according to the
        neural network architecture
        :param input_biases:
        :return: {True, False}
        """
        if(input_biases == None):
            for i in range(1, len(self.architecture)):
                self.bias.append(np.matrix(np.random.uniform(BIAS_LOW, BIAS_HIGH, (1, self.architecture[i]))))
        else:
            for layer, bias_layer in zip(self.architecture[1:], input_biases):
                if(bias_layer.shape[1] != layer):
                    print("Set Bias is not the same size of the architecture of the program!")
                    return False
            self.bias = input_biases
        return True

    def check_input(self, input_layer):
        """
        This makes sure that the input_layer is the same size as the input layer in
        the neural network
        :param input_layer:
        :return: {True, False}
        """
        if(len(input_layer) != self.architecture[0]):
            return False
        else:
            return True

    def activation_function(self, input_activation, input_activation_type):
        """
        This function will take the input and put it through the activation function.
        Example activations are sigmoid, relu, leaky relu, etc.
        :param input_activation:
        :param input_activation_type:
        :return: {input_activation after being put through the activation}
        """
        if(input_activation_type == ActivationFunction.sigmoid):
            output_activation = self.sigmoid_activation(input_activation, PropagationType.forward)
        elif(input_activation_type == ActivationFunction.relu):
            output_activation = self.relu_activation(input_activation, PropagationType.forward)
        elif(input_activation_type == ActivationFunction.leaky_relu):
            output_activation = self.leaky_relu_activation(input_activation, PropagationType.forward)
        else:
            print("ActivationType is not available please fix!")
            sys.exit()

        return output_activation

    def dev_activation_function(self, input_activation, input_activation_type):
        """
        This function will take the input and put it through the derivative of the activation function
        :param input_activation:
        :param input_activation_type:
        :return: {input_activation after being put through the activation}
        """
        if(input_activation_type == ActivationFunction.sigmoid):
            output_derivative_activation = self.sigmoid_activation(input_activation, PropagationType.backward)
        elif(input_activation_type == ActivationFunction.relu):
            output_derivative_activation = self.relu_activation(input_activation, PropagationType.backward)
        elif(input_activation_type == ActivationFunction.leaky_relu):
            output_derivative_activation = self.leaky_relu_activation(input_activation, PropagationType.backward)
        else:
            print("ActivationType is not available please fix!")
            sys.exit()

        return output_derivative_activation

    def forward_propagation(self, input_layer):
        """
        This function will propagate the input layer forward to the end. output_activation will contain all activations
        from each layer and then return output_activation
        :param input_layer:
        :return: {the output to the neural network}
        """
        output_activation = []

        check = self.check_input(input_layer)
        if(check):
            activation_layer = np.matrix(input_layer)
            output_activation.append(activation_layer)
            for layer_num in range(0, len(self.architecture)-1):
                activation_layer = self.activation_function((np.matrix(activation_layer) * np.matrix(self.weights[layer_num])) + self.bias[layer_num],
                                                            ACTIVATION)
                output_activation.append(activation_layer)
            return output_activation
        else:
            print("Input is not the same size as the neural network input layer")
            sys.exit()

    def backward_propagation(self, input_error, input_forward_activations):
        """
        This function will take in the error from the cost_function and then propagate
        the error backward and let the neural network learn
        :param input_error:
        :param input_forward_activations:
        :return: {the error in the input_layer}
        """
        delta_activation = [input_error]
        delta_bias = []
        delta_weights = []
        for i in range(-1, (-1*len(self.architecture)), -1):
            z = (input_forward_activations[i-1]*self.weights[i]) + self.bias[i]
            delta_bias.insert(0, np.multiply(self.dev_activation_function(z, ACTIVATION), delta_activation[0]))
            delta_weights.insert(0, input_forward_activations[i-1].T * np.multiply(self.dev_activation_function(z, ACTIVATION), delta_activation[0]))
            delta_activation.insert(0, (self.weights[i] * np.multiply(self.dev_activation_function(z, ACTIVATION), delta_activation[0]).T).T)

        for i in range(0, len(self.bias)):
            self.bias[i] = np.add(self.bias[i], delta_bias[i])
        self.weights = np.add(self.weights, delta_weights)

        return delta_activation

    def cost_function(self, input_final_output, input_real_output):
        """
        This function takes the output from the forward_propagation function (input_final_output)
        and the actual output we should see (input_real_output) and find the error. The cost function is
        (f(x) - input_real_output)^2 * LEARNING RATE where f(x) = input_final_output array
        :param input_final_output:
        :param input_real_output:
        :return: {the error from the input_final_output and the actual output}
        """
        return (input_final_output - input_real_output) * LEARNING_RATE

    def train(self, input_layer, input_real_output):
        """
        This function does all the training for the network and lets the network learn from
        it's own errors
        :param input_layer:
        :param input_real_output:
        :return: This only trains the network and has no return
        """
        forward_propagations = self.forward_propagation(input_layer)
        error = self.cost_function(forward_propagations[-1], input_real_output)
        self.backward_propagation(error, forward_propagations)

    def write_file(self):
        """
        This will write out the bias and the weights of the neural network
        :param: Writes the biases and weights from this class
        :return: {No return}
        """
        output_file = str(os.getcwd()) + "\\" + str(datetime.now().strftime("%Y-%m-%d_%H-%M-%S")) + ".txt"
        file = open(output_file, "w")
        file.write("Bias:\n" + str(self.bias) + "\n")
        file.write("Weights:\n" + str(self.weights) + "\n")
        file.close()

    def read_file(self, input_file_path):
        """
        Reads the file from the file path and then uses it to set the weights and biases in
        the neural network
        :param input_file_path:
        :return: {True, False}
        """
        pass


def xor_test():
    output = [[1, 1], [0, 1], [1, 0], [1, 1]]
    input = [[1, 1], [0, 0], [0, 1], [1, 0]]

    nn = NeuralNetwork(NEURAL_NETWORK_LAYERS)
    nn.set_biases(None)
    # nn.set_biases([np.matrix([[1, 1]]), np.matrix([[1, 1]])])
    nn.set_weights(None)
    # nn.set_weights([np.matrix([[1, 1], [1, 1]]), np.matrix([[1, 1], [1, 1]])])

    FULL = 50000

    for i in range(0, FULL):
        print("{:.2f}%".format(i/FULL*100))
        random_number = np.random.randint(0, 4)
        nn.train(input[random_number], output[random_number])

    print("Input [1, 1] Output: ", nn.forward_propagation(input[0])[-1], "Real Output: ", output[0])
    print("Input [0, 0] Output: ", nn.forward_propagation(input[1])[-1], "Real Output: ", output[1])
    print("Input [0, 1] Output: ", nn.forward_propagation(input[2])[-1], "Real Output: ", output[2])
    print("Input [1, 0] Output: ", nn.forward_propagation(input[3])[-1], "Real Output: ", output[3])

def read_file_test():
    nn = NeuralNetwork(NEURAL_NETWORK_LAYERS)
    nn.set_biases(None)
    nn.set_weights(None)
    nn.write_file()

def main():
    # xor_test()
    read_file_test()

if __name__ == '__main__':
    main()
